provider "aws" {
  region = "us-east-1"
  
}
resource "aws_instance" "myinstance" {
  ami = "ami-04b70fa74e45c3917"
  instance_type = "t2.micro"
  key_name = "id_rsa"
  vpc_security_group_ids = [ "sg-0000625000575beca" ]
  tags = {
    Name = "test-spot"
  }
}